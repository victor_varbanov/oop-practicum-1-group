//
// Created by viktor on 4/27/2021.
//

#include "course.hpp"

Course::Course(string name, Person *teacher, double grade) : name(name), grade(grade) {
    this->teacher = teacher->clone();
}