//
// Created by viktor on 4/27/2021.
//

#ifndef OOP_PRACTICUM_1_GROUP_COURSE_HPP
#define OOP_PRACTICUM_1_GROUP_COURSE_HPP

#include <string>
#include "person.hpp"

using namespace std;


class Course {
    string name;
    Person* teacher;
    double grade;
public:
    Course(string name, Person* teacher, double grade);
};


#endif //OOP_PRACTICUM_1_GROUP_COURSE_HPP
