//
// Created by viktor on 4/27/2021.
//

#ifndef OOP_PRACTICUM_1_GROUP_PERSON_HPP
#define OOP_PRACTICUM_1_GROUP_PERSON_HPP

#include <string>

using namespace std;

class Person {
private:
    string name;
    string familyName;
    unsigned int age;
    string city;
    string street;
public:
    Person(string name = "", string familyName = "", unsigned int age = 0, string city = "", string street = "");

    virtual Person *clone() = 0;
};


#endif //OOP_PRACTICUM_1_GROUP_PERSON_HPP
