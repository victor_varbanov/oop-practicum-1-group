//
// Created by viktor on 4/27/2021.
//

#include "teacher.hpp"

Teacher::Teacher(int name,
                 int familyName,
                 unsigned int age,
                 int city,
                 int street,
                 unsigned int experience,
                 double paycheck,
                 unsigned int numberOfCourses
) : Person(name, familyName, age, city, street),
    experience(experience),
    paycheck(paycheck),
    numberOfCourses(numberOfCourses) {}

Person *Teacher::clone() {
    return new Teacher(*this);
}