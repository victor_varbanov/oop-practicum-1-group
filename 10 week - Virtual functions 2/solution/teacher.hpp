//
// Created by viktor on 4/27/2021.
//

#ifndef OOP_PRACTICUM_1_GROUP_TEACHER_HPP
#define OOP_PRACTICUM_1_GROUP_TEACHER_HPP


class Teacher : public Person {
private:
    unsigned int experience;
    double paycheck;
    unsigned int numberOfCourses;
public:
    Teacher(string name ="",
            string familyName="",
            unsigned int age=0,
            string city="",
            string street="",
            unsigned int experience,
            double paycheck,
            unsigned int numberOfCourses
            );
    Person* clone() override;

};


#endif //OOP_PRACTICUM_1_GROUP_TEACHER_HPP
