//
// Created by viktor on 5/18/2021.
//

#include "bomber.hpp"
#include <iostream>

using namespace std;

void Bomber::printInfo() {
    cout << "This bomber has " << bombs.size() << "left and weights " << weight << " tons!" << endl;
}

Bomber::~Bomber() {
    for (auto &bomb : bombs) {
        delete bomb;
    }
}

void Bomber::attack() {
    if (!bombs.empty()) {
        weight -= 2;
        cout << "Bomb dropped!" << endl;
        delete bombs[bombs.size() - 1];
        bombs.pop_back();
    } else {
        cout << "The bomber has no bombs left!" << endl;
    }
}