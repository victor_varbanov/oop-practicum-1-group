//
// Created by viktor on 5/18/2021.
//

#ifndef OOP_PRACTICUM_1_GROUP_BOMBER_HPP
#define OOP_PRACTICUM_1_GROUP_BOMBER_HPP

#include "starship.hpp"
#include <vector>
#include <string>
#include "motherShip.hpp"

class Bomber : Starship {
    struct Bomb {
        int damage = 10;
    };

    vector<Bomb *> bombs;

    Bomber() : Starship("Starship", 40) {
        for (int i = 0; i < 10; i++) {
            bombs.emplace_back(new Bomb());
        }
    }

public:

    Bomber &operator=(Bomber &bomber) = delete;

    Bomber(Bomber &bomber) = delete;

    ~ Bomber() override;

    void printInfo() override;

    void attack() override;

    friend class MotherShip;
};

#include "bomber.cpp"

#endif //OOP_PRACTICUM_1_GROUP_BOMBER_HPP
