//
// Created by viktor on 5/18/2021.
//

#include "dropship.hpp"

void Dropship::attack() {
    weight -= 10;
    freeMemory();
}

void Dropship::freeMemory() {
    for (auto &i : drones) {
        delete i;
    }

    drones.clear();
    size = 0;
}

Dropship::~Dropship() {
    freeMemory();
}

void Dropship::printInfo() {
    cout << "This dropship has " << size << " drones left and weights " << weight << " tons!" << endl;
}