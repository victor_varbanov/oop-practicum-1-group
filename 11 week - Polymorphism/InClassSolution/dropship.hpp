//
// Created by viktor on 5/18/2021.
//

#ifndef OOP_PRACTICUM_1_GROUP_DROPSHIP_HPP
#define OOP_PRACTICUM_1_GROUP_DROPSHIP_HPP

#include "starship.hpp"
#include <vector>
#include "motherShip.hpp"

class Dropship : Starship {
    struct CombatDrone {
        int damage = 2;
    };

    void freeMemory();

    vector<CombatDrone *> drones;

    int size;

    Dropship() : Starship("Dropship", 30) {
        for (int i = 0; i < 20; i++) {
            drones.emplace_back(new CombatDrone());
        }
        size = 20;
    }

public:
    Dropship &operator=(Dropship &combatDrone) = delete;

    Dropship(Dropship &dropship) = delete;

    ~Dropship() override;

    void attack() override;

    void printInfo() override;

    friend class MotherShip;
};

#include "dropship.cpp"

#endif //OOP_PRACTICUM_1_GROUP_DROPSHIP_HPP
