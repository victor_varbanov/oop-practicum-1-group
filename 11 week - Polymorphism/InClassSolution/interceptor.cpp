//
// Created by viktor on 5/18/2021.
//

#include "interceptor.hpp"
#include <iostream>

using namespace std;

void Interceptor::attack() {
    timesBombed++;
    cout << "The Interceptor attacked" << endl;
}

void Interceptor::printInfo() {
    cout << "This inteceptor has attacked " << timesBombed << "times until now!" << endl;
}