//
// Created by viktor on 5/18/2021.
//

#ifndef OOP_PRACTICUM_1_GROUP_INTERCEPTOR_HPP
#define OOP_PRACTICUM_1_GROUP_INTERCEPTOR_HPP

#include "starship.hpp"
#include "motherShip.hpp"

class Interceptor : Starship {
    Interceptor() : Starship("Interceptor", 10), timesBombed(0) {}

    unsigned int timesBombed;
public:
    Interceptor &operator=(Interceptor &) = delete;

    Interceptor(Interceptor &) = delete;

    ~Interceptor() override = default;

    void printInfo() override;

    void attack() override;

    friend class MotherShip;
};

#include "interceptor.cpp"
#endif //OOP_PRACTICUM_1_GROUP_INTERCEPTOR_HPP
