//
// Created by viktor on 5/18/2021.
//

#ifndef OOP_PRACTICUM_1_GROUP_MOTHERSHIP_HPP
#define OOP_PRACTICUM_1_GROUP_MOTHERSHIP_HPP

#include "starship.hpp"
#include "iostream"

using namespace std;

class MotherShip {

    Starship **hangar = nullptr;
    unsigned int size = 0;
public:
    static MotherShip getInstance() {
        static MotherShip motherShip;
        return motherShip;
    }

    template<typename Ship>
    Starship *addToHangar() {
        if (hangar == nullptr) {
            hangar = new Starship *[20];
        }
        if (size < 20) {
            hangar[size] = new Ship();
            size++;
            return hangar[size - 1];
        } else {
            cout << "Hangar is full already!" << endl;
            return nullptr;
        }
    }

    void printInformation() {
        for (int i = 0; i < size; i++) {
            hangar[i]->printInfo();
        }
    }

    ~MotherShip() {
        for (int i = 0; i < size; i++) {
            delete hangar[i];
        }
        delete[] hangar;
    }

    MotherShip &operator=(MotherShip &) = delete;
};


#endif //OOP_PRACTICUM_1_GROUP_MOTHERSHIP_HPP
