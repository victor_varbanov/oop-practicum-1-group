//
// Created by viktor on 5/18/2021.
//

#ifndef OOP_PRACTICUM_1_GROUP_STARSHIP_HPP
#define OOP_PRACTICUM_1_GROUP_STARSHIP_HPP

#include <string>

class Starship {
    std::string type;

protected:
    double weight;

    Starship(std::string type, double weight) {
        this->type = type;
        this->weight = weight;
    }

public:
    virtual ~Starship() = default;

    virtual void attack() = 0;

    virtual void printInfo() = 0;

    std::string getType() {
        return type;
    }
};

#endif //OOP_PRACTICUM_1_GROUP_STARSHIP_HPP
