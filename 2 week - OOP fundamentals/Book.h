//
// Created by viktor.varbanov on 3/2/2021.
//

#ifndef UNTITLED_BOOK_H
#define UNTITLED_BOOK_H

class Book {
public:
    char title[250];
    char author[250];
    char genre[250];
    double price;
    int sales;

    void init();
    void print();
};

#endif //UNTITLED_BOOK_H
