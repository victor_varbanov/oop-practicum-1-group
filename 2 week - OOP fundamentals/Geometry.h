//
// Created by viktor.varbanov on 3/2/2021.
//

#ifndef UNTITLED_GEOMETRY_H
#define UNTITLED_GEOMETRY_H


class Triangle {
public:
    int a;
    int b;
    int c;
    int hc;

    int getParameter();

    int getArea();
};


#endif //UNTITLED_GEOMETRY_H
