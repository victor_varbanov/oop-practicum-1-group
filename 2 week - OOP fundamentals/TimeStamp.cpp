//
// Created by viktor.varbanov on 3/2/2021.
//

#include "TimeStamp.h"
#include <iostream>

using namespace std;

void TimeStamp::getTimeStampFormatted(int seconds) {
    hours = seconds / 3600;
    min = seconds % 3600 / 60;
    sec = seconds % 60;

    cout << hours << ":" << min << ":" << sec << endl;
}

int TimeStamp::calculateSeconds(TimeStamp timeStamp) {
    int seconds = 0;
    seconds += min * 60;
    seconds += hours * 3600;
    seconds += sec;
    seconds += timeStamp.sec;
    seconds += timeStamp.hours * 3600;
    seconds += timeStamp.min * 60;
    return seconds;
}

