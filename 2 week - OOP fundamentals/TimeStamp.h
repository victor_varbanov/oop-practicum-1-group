//
// Created by viktor.varbanov on 3/2/2021.
//

#ifndef UNTITLED_TIMESTAMP_H
#define UNTITLED_TIMESTAMP_H


class TimeStamp {
public:
    int hours;
    int min;
    int sec;

    void getTimeStampFormatted(int seconds);

    int calculateSeconds(TimeStamp timeStamp);
};

#endif //UNTITLED_TIMESTAMP_H
