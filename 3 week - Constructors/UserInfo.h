//
// Created by Viktor Varbanov on 9.03.21.
//

#ifndef OOP_PRACTICUM_1_GROUP_USERINFO_H
#define OOP_PRACTICUM_1_GROUP_USERINFO_H

#include <iostream>
#include <string.h>

using namespace std;
class UserInfo{
private:
    char username[250];
    char password[250];
    int age;

    const int number=8;
public:

    UserInfo(char pass[]="default password", char username[]="Gergana", int age=7): number(7),age(age){
        strncpy(this->password,pass,250);
        strncpy(this->username,username,250);
        this->age=age;
    }

    UserInfo(const UserInfo& copuUser){
        strncpy(password,copuUser.password,250);
        strncpy(username, copuUser.username, 250);
        age=copuUser.age;
    }

    void print(){
        cout<<password<<" "<<username<<" "<<age<<endl;
    }
};

#endif //OOP_PRACTICUM_1_GROUP_USERINFO_H
