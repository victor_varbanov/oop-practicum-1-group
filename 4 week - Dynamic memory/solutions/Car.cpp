//
// Created by Viktor Varbanov on 14.03.21.
//

#include "Car.hpp"
#include <iostream>

using namespace std;

Car::Car(const char model[], const char number[], const char color[]) {
    strncpy(this->model, model, 250);
    strncpy(this->number, number, 250);
    strncpy(this->color, color, 250);
}

Car::Car(const Car &copyCar) {
    strncpy(this->model, copyCar.model, 250);
    strncpy(this->number, copyCar.number, 250);
    strncpy(this->color, copyCar.color, 250);
}

void Car::setModel(char newModel[]) {

    if (newModel == "honda") {
        cout << "Ne iskam honda" << endl;
    } else strncpy(model, newModel, 250);
}

void Car::print() {
    cout << model << " " << number << " " << color << endl;
}