#ifndef OOP_PRACTICUM_1_GROUP_CAR_HPP
#define OOP_PRACTICUM_1_GROUP_CAR_HPP

#include "string.h"

class Car {
private:
    char number[250];
    char color[250];
    char model[250];
public:
    Car(const char model[] = "model", const char number[] = "number", const char color[] = "color");

    Car(const Car &copyCar);

    void print();

    void setModel(char newModel[]);

    char *getColor() {
        return color;
    }

    char *getNumber() {
        return number;
    }

    char *getModel() {
        return model;
    }
};

#endif //OOP_PRACTICUM_1_GROUP_CAR_HPP
