//
// Created by Viktor Varbanov on 14.03.21.
//

#ifndef OOP_PRACTICUM_1_GROUP_LAPTOP_HPP
#define OOP_PRACTICUM_1_GROUP_LAPTOP_HPP

#include "Processor.hpp"

class Laptop {
public:
    Laptop();

    Laptop(const char *, const double &); // create Laptop by name and price

    Laptop(const char *,
           const double &,
           const double &,
           const Processor &,
           const double &,
           const int &);

    Laptop(const Laptop &);

    Laptop &operator=(const Laptop &);

    ~Laptop();

    // methods of the class Laptop::operator==(const Laptop&)
    bool operator==(const Laptop &) const;

    bool operator!=(const Laptop &) const;

    bool operator<(const Laptop &) const;

    bool operator>(const Laptop &) const;

    void printLaptopInformation() const;

private:
    char *brandAndModel;

    double price;

    double screenInches;

    Processor processor;

    double ram;

    int batteryCapacity;

    void copyFrom(const Laptop &);

    void deallocateMemory();
};


#endif //OOP_PRACTICUM_1_GROUP_LAPTOP_HPP
