#ifndef OOP_PRACTICUM_1_GROUP_PARKING_HPP
#define OOP_PRACTICUM_1_GROUP_PARKING_HPP

#include "Car.hpp"

class Parking {
private:
    char *name = nullptr;
    Car *cars = nullptr;
    int count;
public:
    Parking();

    Parking(const char *name, Car *cars, const int &count);

    Parking(const Parking &newParking);

    ~Parking();

    void setCars(Car newCars[], const int &newCount);

    void setName(const char *newName);

    int getCount();

    Car *getCars();

    char *getName();

    void PrintAllCars();

    void addCar(const Car &newCar);

    void removeCar(const int &index);

    void addCarAtIndex(const int &index, const Car &newCar);

    char *getModel(const char *number);
};

#endif //OOP_PRACTICUM_1_GROUP_PARKING_HPP
