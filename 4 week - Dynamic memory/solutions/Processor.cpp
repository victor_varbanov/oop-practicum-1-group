//
// Created by Viktor Varbanov on 14.03.21.
//

#include "Processor.hpp"
#include <iostream>

using namespace std;

Processor::Processor(const double &frequency, const unsigned &cores, const double &cashMemory)
        : frequency(frequency), cores(cores), cashMemory(cashMemory) {
}

void Processor::printProcessorInformation() const {
    cout << "Frequency: " << this->frequency << " GHz" << endl;
    cout << this->cores << " cores" << endl;
    cout << "Cash: " << this->cashMemory << endl;
}

