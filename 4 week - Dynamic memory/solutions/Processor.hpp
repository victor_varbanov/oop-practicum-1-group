//
// Created by Viktor Varbanov on 14.03.21.
//

#ifndef OOP_PRACTICUM_1_GROUP_PROCESSOR_HPP
#define OOP_PRACTICUM_1_GROUP_PROCESSOR_HPP


class Processor {
public:
    Processor() = default;

    Processor(const Processor &) = default;

    Processor &operator=(const Processor &) = default;

    ~Processor() = default;

    Processor(const double &, const unsigned &, const double &);

    void printProcessorInformation() const;

private:
    double frequency;
    unsigned cores;
    double cashMemory;
};


#endif //OOP_PRACTICUM_1_GROUP_PROCESSOR_HPP
