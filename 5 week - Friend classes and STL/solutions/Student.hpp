//
// Created by Viktor Varbanov on 16.03.21.
//

#ifndef OOP_PRACTICUM_1_GROUP_STUDENT_HPP
#define OOP_PRACTICUM_1_GROUP_STUDENT_HPP

#include <string.h>

class Student
{
    char *name = nullptr;
    int reg_number = -1;

    void copy(const Student &other);

public:
    bool hasSubject = false;

    Student(int reg_number = -1, const char *name = nullptr, bool has_subject = false);

    Student(const Student &other);

    Student& operator=(const Student &other);

    ~Student();


    void print();

    friend class University;
};


#endif //OOP_PRACTICUM_1_GROUP_STUDENT_HPP
