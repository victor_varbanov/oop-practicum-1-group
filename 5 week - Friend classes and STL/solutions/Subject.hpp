//
// Created by Viktor Varbanov on 21.03.21.
//

#ifndef OOP_PRACTICUM_1_GROUP_SUBJECT_HPP
#define OOP_PRACTICUM_1_GROUP_SUBJECT_HPP

#include "University.hpp"

class Subject {
    static University *uni;

    int *regNumArr = nullptr;
    int size = 0;
public:
    static void setUni(University *uni);

    Subject();

    ~Subject();

    void addStudentNumber(int reg_num);

    static void printStudentsWithoutSubject();

    Student &operator[](int i);

    //Subject(const Subject &other)
    //Subject& operator=(const Subject &other)
};

#endif //OOP_PRACTICUM_1_GROUP_SUBJECT_HPP
