//
// Created by Viktor Varbanov on 16.03.21.
//

#ifndef OOP_PRACTICUM_1_GROUP_UNIVERSITY_HPP
#define OOP_PRACTICUM_1_GROUP_UNIVERSITY_HPP

#include "Student.hpp"

class University
{
    Student **students = nullptr;
    int size = 0;
    int capacity = 0;

    void erase();

    void copy(const University &other);
public:
    University();

    University(const University &other);

    University& operator=(const University &other);

    ~University();

    void addStudent(const char *name);

    Student& operator[](int i);

    int count();

    University operator+(const University &other);

};

#endif //OOP_PRACTICUM_1_GROUP_UNIVERSITY_HPP
