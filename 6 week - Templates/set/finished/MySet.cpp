//
// Created by Viktor Varbanov on 30.03.21.
//

#include <iostream>
#include "MySet.hpp"

using namespace std;

template<typename T>
void MySet<T>::copy(const MySet<T> &s) {
    this->size = s.size;
    this->capacity = s.capacity;
    this->data = new T[this->capacity];

    for (int i = 0; i < this->size; i++) {
        this->data[i] = s.data[i];
    }
}

template<typename T>
void MySet<T>::erase() {
    delete[] this->data;
}

template<typename T>
void MySet<T>::resize() {
    this->capacity *= 2;
    T *oldData = data;
    this->data = new T[this->capacity];

    for (int i = 0; i < this->size; i++) {
        this->data[i] = oldData[i];
    }

    delete[] oldData;
}

template<typename T>
MySet<T>::MySet() {
    this->capacity = SET_INITIAL_CAPACITY;
    this->size = 0;
    this->data = new T[this->capacity];
}

template<typename T>
MySet<T>::MySet(const MySet<T> &s) {
    copy(s);
}


template<typename T>
MySet<T> &MySet<T>::operator=(const MySet<T> &other) {
    if (&other != this) {
        erase();
        copy(other);
    }
    return *this;
}

template<typename T>
MySet<T>::~MySet<T>() {
    erase();
}

template<typename T>
int MySet<T>::getSize() const {
    return size;
}


template<typename T>
bool MySet<T>::add(T element) {
    if (contains(element)) {
        return false;
    }

    if (size >= capacity) {
        resize();
    }

    data[size++] = element;

    return true;
}

template<typename T>
bool MySet<T>::contains(T element) const {
    for (int i = 0; i < size; i++) {
        if (data[i] == element) {
            return true;
        }
    }

    return false;
}

template<typename T>
bool MySet<T>::isEmpty() {
    return (size == 0);
}


template<typename T>
bool MySet<T>::remove(T element) {
    if (!contains(element)) {
        return false;
    }

    for (int i = 0; i < size; i++) {
        if (data[i] == element) {
            for (int j = i; j < size - 1; j++) {
                data[j] = data[j + 1];
            }
            size--;
            data[size] = T();
            break;
        }
    }

    return true;
}

template<typename T>
T MySet<T>::operator[](const int index) const {
    if (index >= size) {
        cerr << "Index out of bound" << endl;
    }
    return data[index];
}


template<typename T>
bool MySet<T>::operator==(const MySet<T> &other) const {
    if (size != other.size) {
        return false;
    }

    if (capacity != other.capacity) {
        return false;
    }

    for (int i = 0; i < size; i++) {
        if (!contains(other.data[i])) {
            return false;
        }
    }

    return true;
}

template<typename T>
MySet<T> MySet<T>::operator+(const MySet<T> &other) const {
    MySet<T> set = *this;

    for (int i = 0; i < other.size; i++) {
        set.add(other.data[i]);
    }

    return set;
}

template<typename T>
MySet<T> MySet<T>::operator*(const MySet<T> &other) const {
    MySet<T> set;

    for (int i = 0; i < this->size; i++) {
        if (other.contains(data[i])) {

            set.add(data[i]);
        }
    }

    return set;
}

template<typename T>
MySet<T> MySet<T>::operator-(const MySet<T> &other) const {
    MySet<T> set = *this;

    for (int i = 0; i < other.size; i++) {
        set.remove(other.data[i]);
    }

    return set;
}

template<typename T>
ostream &operator<<(ostream &os, const MySet<T> &set) {
    for (int i = 0; i < set.getSize(); i++) {
        os << set[i] << " ";
    }

    return os;
}