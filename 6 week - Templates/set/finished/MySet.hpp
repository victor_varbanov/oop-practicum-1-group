//
// Created by Viktor Varbanov on 30.03.21.
//

#ifndef OOP_PRACTICUM_1_GROUP_MYSET_HPP
#define OOP_PRACTICUM_1_GROUP_MYSET_HPP

using namespace std;

const int SET_INITIAL_CAPACITY = 4;

template<typename T>
class MySet {

private:
    T *data;
    int size;
    int capacity;

    void copy(const MySet<T> &s);

    void erase();

    void resize();

public:
    MySet<T>();

    MySet<T>(const MySet<T> &s);

    MySet<T> &operator=(const MySet<T> &other);

    ~MySet<T>();

    int getSize() const;

    bool add(T element);

    bool contains(T element) const;

    bool isEmpty();

    bool remove(T element);

    T operator[](const int index) const;

    bool operator==(const MySet<T> &other) const;

    MySet<T> operator+(const MySet<T> &other) const;

    MySet<T> operator*(const MySet<T> &other) const;

    MySet<T> operator-(const MySet<T> &other) const;

    template<typename S>
    friend ostream &operator<<(ostream &os, const MySet<S> &set);
};


#endif //OOP_PRACTICUM_1_GROUP_MYSET_HPP
