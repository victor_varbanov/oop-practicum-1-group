//
// Created by Viktor Varbanov on 30.03.21.
//

#include "Stack.hpp"

template<typename T>
Stack<T>::Stack(): capacity(0), size(0), vector(Vector<T>()) {}

template<typename T>
Stack<T>::Stack(int capacity) : capacity(capacity), size(0) {
    vector = Vector<T>(capacity);
}

template<typename T>
Stack<T>::Stack(const Stack<T> &rhs) {
   copyFrom(rhs);
}

template<typename T>
void Stack<T>::copyFrom(Stack<T> & rhs) {
    size = rhs.size;
    capacity = rhs.capacity;
    vector = rhs.vector;
}

template<typename T>
void Stack<T>::push(T element) {
    vector.push(element);
}

template<typename T>
T Stack<T>::pop() {
    return vector.pop();
}

template<typename T>
int Stack<T>::getSize() {
    return size;
}

template<typename T>
Stack<T> &Stack<T>::operator=(const Stack<T> &rhs) {
    if (this != &rhs) {
        copyFrom(rhs);
    }
    return *this;
}