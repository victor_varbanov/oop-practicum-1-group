//
// Created by Viktor Varbanov on 30.03.21.
//

#ifndef OOP_PRACTICUM_1_GROUP_STACK_HPP
#define OOP_PRACTICUM_1_GROUP_STACK_HPP

#include "../vector/inclass/Vector.hpp"

template<typename T>
class Stack {
private:
    int size;
    int capacity;
    Vector<T> vector;
    void copyFrom(Stack<T>&);
public:
    Stack();

    ~Stack()=default;

    Stack<T> &operator=(const Stack<T>&);

    Stack<T>(int);

    Stack<T>(const Stack &);

    void push(T);

    T pop();

    int getSize();
};


#endif //OOP_PRACTICUM_1_GROUP_STACK_HPP
