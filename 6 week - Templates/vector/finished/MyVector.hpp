//
// Created by Viktor Varbanov on 30.03.21.
//

#ifndef OOP_PRACTICUM_1_GROUP_MYVECTOR_HPP
#define OOP_PRACTICUM_1_GROUP_MYVECTOR_HPP

#include <iostream>

template <typename T>
class MyVector
{
public:
    MyVector();
    MyVector(const size_t);
    MyVector(const MyVector&);
    MyVector<T>& operator=(const MyVector&);
    ~MyVector();

    void pushBack(const T);
    T popBack();
    void printVector() const;

    size_t getSize() const;
private:
    T* values;
    size_t size;
    size_t capacity;

    void copyFrom(const MyVector&);
    void deallocateValues();
    void resize();
};

template <typename T>
MyVector<T>::MyVector()
        : values(nullptr)
        , size(0)
        , capacity(0)
{
}

template <typename T>
MyVector<T>::MyVector(const size_t capacity)
        : values(new T [capacity])
        , size(0)
        , capacity(capacity)
{
}

template <typename T>
MyVector<T>::MyVector(const MyVector<T>& other)
{
    copyFrom(other);
}

template <typename T>
MyVector<T>& MyVector<T>::operator=(const MyVector<T>& other)
{
    if (this != &other)
    {
        deallocateValues();
        copyFrom(other);
    }
    return *this;
}

template <typename T>
MyVector<T>::~MyVector()
{
    deallocateValues();
}

template <typename T>
void MyVector<T>::pushBack(const T value)
{
    if (this->values == nullptr)
    {
        this->capacity = 1;
        this->values = new T [this->capacity];
    }
    else
    {
        resize();
    }

    this->values[this->size] = value;
    this->size++;
}

template <typename T>
T MyVector<T>::popBack()
{
    if (this->size > 0)
    {
        T last = this->values[this->size - 1];
        this->size--;
        T value;
        this->values[this->size] = value;
        return last;
    }
    T defaultResult;
    return defaultResult;
}

template <typename T>
void MyVector<T>::printVector() const
{
    for (size_t i = 0; i < this->size; i++)
    {
        this->values[i].print();
    }
    std::cout << std::endl;
}

template <typename T>
size_t MyVector<T>::getSize() const
{
    return this->size;
}

template <typename T>
void MyVector<T>::copyFrom(const MyVector<T>& other)
{
    this->capacity = other.capacity;
    this->size = other.size;
    this->values = new T [other.capacity];
    for (size_t i = 0; i < other.size; i++)
    {
        this->values[i] = other.values[i];
    }
}

template <typename T>
void MyVector<T>::deallocateValues()
{
    if (this->values != nullptr)
    {
        delete[] this->values;
    }
}

template <typename T>
void MyVector<T>::resize()
{
    if (this->size == this->capacity)
    {
        this->capacity *= 2;
        T* placeholder = new T [this->capacity];
        for (size_t i = 0; i < this->size; i++)
        {
            placeholder[i] = this->values[i];
        }
        deallocateValues();
        this->values = placeholder;
    }
}

#endif //OOP_PRACTICUM_1_GROUP_MYVECTOR_HPP
