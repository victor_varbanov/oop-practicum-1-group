//
// Created by Viktor Varbanov on 30.03.21.
//

#include <stdexcept>
#include "Vector.hpp"

template<typename T>
Vector<T>::Vector() : size(0), capacity(0), arr(nullptr) {}

template<typename T>
Vector<T>::Vector(int capacity) : capacity(capacity), size(0) {
    arr = new T[capacity];
}

template<typename T>
Vector<T>::Vector(const Vector<T> &lhs) {
    copyFrom(lhs);
}

template<typename T>
Vector<T> &Vector<T>::operator=(const Vector<T> &lhs) {
    if (this != &lhs) {
        deallocate();
        copyFrom(lhs);
    }
    return *this;
}

template<typename T>
void Vector<T>::copyFrom(const Vector<T> &lhs) {
    this->capacity = lhs.capacity;
    this->size = lhs.size;
    this->arr = new T[capacity];
    for (int i = 0; i < size; i++) {
        this->arr[i] = lhs.arr[i];
    }
}

template<typename T>
Vector<T>::~Vector<T>() {
    deallocate();
}

template<typename T>
void Vector<T>::deallocate() {
    if(arr != nullptr) delete[] arr;
}

template<typename T>
void Vector<T>::push(T element) {
    if (size == capacity) {
        resize();
    }
    arr[size] = element;
    size++;
}

template<typename T>
void Vector<T>::resize() {
    if (capacity == 0) capacity = 1;
    T *newArr = new T[capacity * 2];
    for (unsigned int i = 0; i < size; i++) {
        newArr[i] = arr[i];
    }
    deallocate();
    capacity *= 2;
    arr = newArr;
}

template<typename T>
int Vector<T>::getSize() {
    return size;
}

template<typename T>
T Vector<T>::pop() {
    if (size == 0) return T();
    T answer = arr[size - 1];
    size--;
    return answer;
}

template<typename T>
void Vector<T>::add(int idx, const T &element) {
    if (idx >= size) {
        throw std::invalid_argument("Vector is not large enough");
    }
    if (size == capacity) capacity *= 2;
    T *newArr = new T[capacity];
    for (int i = 0; i < idx; i++) {
        newArr[i] = arr[i];
    }
    newArr[idx] = element;
    for (int i = idx; i < size; i++) {
        newArr[i + 1] = arr[i];
    }
    deallocate();
    size++;
    arr = newArr;
}

template<typename T>
void Vector<T>::remove(int idx) {
    if (idx >= size) {
        throw std::invalid_argument("Vector is not large enough");
    }

    T *newArr = new T[capacity - 1];
    for (int i = 0; i < idx; i++) {
        newArr[i] = arr[i];
    }
    size--;
    for (int i = idx; i < size; i++) {
        newArr[i] = arr[i + 1];
    }
    deallocate();
    arr = newArr;
}

template<typename T>
void Vector<T>::print() {
    for (int i = 0; i < size; i++) {
        std::cout << arr[i] << " ";
    }
    std::cout << std::endl;
}