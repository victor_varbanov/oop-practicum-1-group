//
// Created by Viktor Varbanov on 30.03.21.
//

#ifndef OOP_PRACTICUM_1_GROUP_VECTOR_HPP
#define OOP_PRACTICUM_1_GROUP_VECTOR_HPP

#include <iostream>

template <typename T>
class Vector {
private:
    T* arr;
    int capacity;
    int size;
    void deallocate();
    void copyFrom(const Vector<T>& lhs);
    void resize();
public:
    Vector();
    Vector(int capacity);
    Vector(const Vector<T>& lhs);
    Vector<T>& operator=(const Vector<T>& lhs);
    ~Vector();
    void push(T);
    int getSize();
    T pop();
    void add(int , const T& element );
    void remove(int);
    void print();
};


#endif //OOP_PRACTICUM_1_GROUP_VECTOR_HPP
