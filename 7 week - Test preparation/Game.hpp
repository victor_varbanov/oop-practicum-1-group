#pragma once;
#include "Player.hpp"
#include <iostream>
class Game
{
    int number;
    Player player;
    void makeGuess()
    {
        int guess = player.makeGuess();
        if (guess == number)
        {
            player.win();
            std::cout << guess << " You win" << std::endl;
        }
        else
            std::cout << guess << " You lose" << std::endl;
    }

public:
    Game(int number, Player &player) : number(number)
    {
        this->player = player;
    };
    void startGame()
    {
        player.decreaseCoins();
        makeGuess();
    }
};