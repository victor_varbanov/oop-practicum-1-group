#pragma once
#include <string>
using namespace std;

class Message
{
    string sender = "";
    string message;
    string reciever = "";

public:
    Message(string sender = "", string message = "", string reciever = "") : sender(sender), reciever(reciever), message(message) {}
    string getMessage() const
    {
        return message;
    }
    string getReciever() const
    {
        return reciever;
    }
    string getSender() const
    {
        return sender;
    }
};