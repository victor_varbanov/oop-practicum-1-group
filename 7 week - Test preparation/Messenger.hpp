#pragma once;
#include "Message.hpp"
#include <iostream>
#include <vector>
using namespace std;

class Messenger
{
    string id;
    vector<Message> in;
    vector<Message> out;

public:
    void sendMessage(const Message &m);
    void recieveMessage(const Message &m);
    void print();
};

void Messenger::sendMessage(const Message &m)
{
    out.push_back(m);
}

void Messenger::recieveMessage(const Message &m)
{
    in.push_back(m);
}

void Messenger::print()
{
    cout << "In: ";
    for (int i = 0; i < in.size(); i++)
    {
        cout << in[i].getMessage() << " ";
    }
    cout << endl
         << "Out: ";
    for (int i = 0; i < out.size(); i++)
    {
        cout << out[i].getMessage() << " ";
    }
}