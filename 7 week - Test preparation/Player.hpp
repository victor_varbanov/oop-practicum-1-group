#pragma once
#include "string"
#include "time.h"
#include <iostream>
class Player
{
    unsigned int coins;
    std::string name;
    static int hackerNumber;

public:
    Player(int coin = 0, std::string name = "") : name(name), coins(coins)
    {
    }
    void decreaseCoins()
    {
        coins--;
    }
    int makeGuess()
    {
        std::cout << hackerNumber << std::endl;
        if (hackerNumber != -1)
        {
            int copy = hackerNumber;
            hackerNumber = -1;
            return copy;
        }
        else
        {
            srand(time(NULL));
            return random() % 100;
        }
    }
    void win()
    {
        coins += 2;
    }
    void hack(int hack)
    {
        std::cout << "there" << std::endl;
        hackerNumber = hack;
    }
};

int Player::hackerNumber = -1;