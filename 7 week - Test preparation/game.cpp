#include "Game.hpp"
#include "Player.hpp"
#include <iostream>
int ourNumber = 42;

int Hacker(Player &player)
{
    player.hack(ourNumber);
}

int main()
{
    std::string input;
    Player ivan(4, "ivan");
    Game game(ourNumber, ivan);
    while (true)
    {
        std::cin >> input;
        if (input == "y")
            game.startGame();
        if (input == "h")
        {
            Hacker(ivan);
            game.startGame();
        }
        if (input == "e")
            break;
    }
    return 0;
}