#include "Message.hpp"
using namespace std;

Message encrypt(const Message &m)
{
    string encrypted = "";
    string message = m.getMessage();
    for (int i = 0; i < message.size(); i++)
    {
        encrypted[i] = message[i] - 1;
    }
    return Message(m.getSender(), encrypted, m.getReciever());
}

Message decrypt(const Message &m)
{
    string encrypted = "";
    string message = m.getMessage();
    for (int i = 0; i < message.size(); i++)
    {
        encrypted[i] = message[i] + 1;
    }
    return Message(m.getSender(), encrypted, m.getReciever());
}