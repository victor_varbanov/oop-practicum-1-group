#pragma once
#include <string>
#include <fstream>
using namespace std;

template <typename T>
class Logger
{
    string filename;

public:
    Logger(string filename = "log.txt") : filename(filename) {}

    void write(const char *output)
    {
        ofstream file;

        file.open(filename, std::ios::app);
        file << output;

        file.close();
    }

    void write(const T *output)
    {
        ofstream file;

        file.open(filename, std::ios::app);
        file << *output;

        file.close();
    }

    void writeLine(const char *output)
    {
        ofstream file;

        file.open(filename, std::ios::app);
        file << output << '\n';

        file.close();
    }

    void writeLine(const T *output)
    {
        ofstream file;

        file.open(filename, std::ios::app);
        file << output << '\n';

        file.close();
    }
};