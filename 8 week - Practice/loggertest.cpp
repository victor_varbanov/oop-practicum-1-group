#include "Logger.hpp"

int main()
{
    Logger<int> logger("./test.txt");

    int a = 7;
    logger.writeLine(&a);
    logger.writeLine(&a);

    logger.write("I am writing to a file");
    logger.writeLine("I am writing to a file 2");
}