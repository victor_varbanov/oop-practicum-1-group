#pragma once
#include <string>
#include <iostream>
using namespace std;

class Animal
{
    string name;

protected:
    int numberOfLegs;

public:
    Animal(string name, int number) : name(name), numberOfLegs(number){};

    void makeSound()
    {
        cout << "I am an animal and I make sounds";
    }
};