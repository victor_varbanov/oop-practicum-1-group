#pragma once
#include "Animal.hpp"

class Dog : public Animal
{
    string poroda;

public:
    Dog(string name, int number, string poroda) : Animal::Animal(name, number)
    {
        this->poroda = poroda;
    }

    void makeSound()
    {
        cout << "Woof";
    }
};