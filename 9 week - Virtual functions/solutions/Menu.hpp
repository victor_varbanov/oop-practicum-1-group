//
// Created by viktor on 4/20/2021.
//

#ifndef OOP_PRACTICUM_1_GROUP_MENU_HPP
#define OOP_PRACTICUM_1_GROUP_MENU_HPP

class Menu {
private:
    Food** foods;
    int size;
    int capacity;

    void copy(const Menu& m);

    void erase();

    void resize();

public:
    Menu(int capacity = 4);

    Menu(const Menu& m);

    Menu& operator=(const Menu& other);

    ~Menu();

    void print();

    const Food* operator[](int index) const;

    void addFood(Food* f);

    void remove(const Food* f);

    void getCheapestFood();

    void consumeFood(const Food* f);
};

#include "Menu.cpp"

#endif //OOP_PRACTICUM_1_GROUP_MENU_HPP
