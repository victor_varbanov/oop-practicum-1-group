//
// Created by viktor on 4/20/2021.
//

#ifndef OOP_PRACTICUM_1_GROUP_PIZZA_HPP
#define OOP_PRACTICUM_1_GROUP_PIZZA_HPP

#include <string>

class Pizza : public Food {
private:
    string type;

public:
    Pizza(const string& type = "N/A",
          const int weight = 450, const int parts = 8,
          const double price = 6.5);

    void print() override;

    void category();

    Food* clone();
};

#include "Pizza.cpp"

#endif //OOP_PRACTICUM_1_GROUP_PIZZA_HPP
