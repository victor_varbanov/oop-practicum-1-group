//
// Created by viktor on 4/20/2021.
//

#ifndef OOP_PRACTICUM_1_GROUP_SALAD_HPP
#define OOP_PRACTICUM_1_GROUP_SALAD_HPP

class Salad : public Food {
private:
    int productsCount;
    bool isSpiced;

public:
    Salad(const int productsCount = 4, const bool isSpiced = true,
          const int weight = 300, const double price = 4.5);

    void category();

    Food* clone();

    void print();
};

#include "Salad.cpp"

#endif //OOP_PRACTICUM_1_GROUP_SALAD_HPP
